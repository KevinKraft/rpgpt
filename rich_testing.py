from rich.console import Console
from rich.markdown import Markdown
import sys

console = Console()


def display_help():
    md_content = """You can add two numbers in Python by using the `+` operator. Here's an example:\n\n```python\nnum1 = 5\nnum2 = 10\nsum = num1 + num2\nprint(sum)\n```\n\nThis will output `15`. In this example, we've assigned the values `5` and `10` to two variables `num1` and `num2` respectively. Then we add these two variables using the `+` operator and store the result in the variable `sum`. Finally, we print the value of `sum`."""
    console.print(Markdown(md_content))
    sys.exit(0)
   

if len(sys.argv) >= 2:
    if sys.argv[1].lower() == "help":
            display_help()