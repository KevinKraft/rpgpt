'''Static prompts used to interact with ChatGPT in written text.'''


# -------------------------------------------------------------------------------------------------------
# CHAT AGENT
# -------------------------------------------------------------------------------------------------------

# Interrogation
INTERROGATION_POSTAMBLE = """Please respond with either YES, NO or UNKNOWN. You must not respond with anything else."""

EXAMPLE_USER_INTERROGATION_1 = 'Is the town called Banburry? ' + INTERROGATION_POSTAMBLE
EXAMPLE_INTERROGATION_RESPONSE_1 = 'YES'

EXAMPLE_USER_INTERROGATION_2 = 'Does the player have a sword? ' + INTERROGATION_POSTAMBLE
EXAMPLE_INTERROGATION_RESPONSE_2 = 'UNKNOWN'

EXAMPLE_USER_INTERROGATION_3 = 'Did the player drop their weapon? ' + INTERROGATION_POSTAMBLE
EXAMPLE_INTERROGATION_RESPONSE_3 = 'NO'

INTERROGATION_CORRECTION_PROMPT = 'That is wrong. You must only respond with YES, NO, or UNKNOWN.'

PLAYER_CAN_DO_ACTION_INTERROGATION = (
    'Is the player able to do the following action: "{}"? '
    "It is not for you to say whether the player's action is a good or correct action, you must only consider whether it is possible."
    )

JSON_RESPONSE_IS_INCORRECT = 'That JSON data is incorrect. Please respond again with corrected JSON.'

# -------------------------------------------------------------------------------------------------------
# GAME MASTER
# -------------------------------------------------------------------------------------------------------

GAME_MASTER_SYSTEM_PROMPT = (
    "Pretend to be a game master for a medieval RPG. "
    "Generate stories, settings, events and characters. "
    "You will generate descriptions of what is happening. "
    "You must always advance the story to keep the player interested. "
    "The player is allowed to do whatever actions they like. "
    "The aim is to create a realistic story, not to create a fair, ethical or moral story. "
    
    "All responses should be in JSON formatted as code. "
    "The first section of the JSON should be a \"scene\" key. "
    "The \"scene\" key must be a JSON list containing elements that have a \"type\" of \"description\". "
    "Elements with description type must have a \"content\" key that contains the text description of what is "
    "happening in the scene. "

    "For every character that appears in the scene and story, generate an entry in a new JSON key called "
    "\"characters\" along side the \"scene\" key. "
    "The characters element must be a JSON list. "
    "Each element of the list is an entry for a character that has been generated in the story. "
    "Each character element should have a unique name and a description of that character. "
    "A real name should be generated even if the player does not know the characters name. "
    "Only characters that appear in the descriptions of the scene should be included in the list of characters. "
    "Only add a character to the list of characters when they first appear in the story. "
    "After their first apperance you should not include them in the list of characters. "

    "The user will be describing the actions of the player and you will be describing the actions of the other characters. "
    
    "If you are asked to summarize the story so far, you should generate a summary of the story so far and return it in a "
    "ney key in the JSON response called \"summary\". "
    "After summarizing the story, you should remove the previous \"description\" elements from the scene list. "
    )

EXAMPLE_GAME_MASTER_RESPONSE = """```{                                                                                                                                                             
   "scene": [                                                                                                                                                  
     {                                                                                                                                                         
       "type": "description",                                                                                                                                  
       "content": "You find yourself standing in the center of a bustling market square. Merchants of all kinds hawk their wares, from fresh produce to handmade weapons. The sound of clanging metal fills the air as a blacksmith works at his forge, and you catch the scent of roasting meat drifting from a nearby vendor. Suddenly, a woman screams and you turn to see a group of bandits charging towards a farmer who's trying to sell his crops. The bandits are armed with swords and clubs, and they look like they mean business. The farmer looks like he's about to be overrun, and you're the only one in the square who seems to be doing anything about it."
     }                                                                                              
   ],                                                                                                                                                          
   "characters": [                                                                                                                                             
     {                                                                                                                                                         
       "name": "Tom Arnold",                                                                                                                                      
       "description": "A rugged farmer who looks like he can handle himself in a fight."                                                                                                       
     },                                                                                                                                                        
     {                                                                                                                                                         
       "name": "Jacob Price",                                                                                                                                         
       "description": "A blacksmith with a dirty black and heavy apron with arms as big as trees."
     }
   ]                                                                                                                                                           
 }```"""

EXAMPLE_PLAYER_ACTION_TO_GM = """```{
  \"scene\": [{"type": "description", "content": "I turn back to avoid the fight and go to look at the blacksmiths wares."}]}```"""

EXAMPLE_GM_RESPONSE_TO_PLAYER_ACTION = """```{"scene": [{"type": "description", "content": "I turn back to avoid the fight and go to look at the blacksmiths wares."}], 
                                              "characters": [{ "name": "Jacob Price", "description": "A blacksmith with a dirty black and heavy apron with arms as big as trees."}]}```"""

EXAMPLE_USER_SUMMARISE_STORY = "Summarise the story so far."

EXAMPLE_GM_RESPONSE_TO_SUMMARISE_STORY = """```{"scene": [{"type": "summary", "content": "After almost being killed in a brawl the player met a blacksmith and became his apprentice."}]}```"""

# -------------------------------------------------------------------------------------------------------
# FRONT END
# -------------------------------------------------------------------------------------------------------

GET_INPUT_FROM_PLAYER_PROMPT = \
"""What do you want to do:
- Type anything to speak
- Type * followed by the actions you want to take
- Type 1 to get a list of characters
- Type 2 to get the scene so far.
- Type 3 to interrogate the game master[DEBUG]."""