import os
from openai import OpenAI

client = OpenAI(api_key=os.getenv('OPENAI_API_KEY'))
from rich.console import Console
from rich.markdown import Markdown


CONSOLE = Console()

class ChatApp:
    def __init__(self):
        # Setting the API key to use the OpenAI API
        self.messages = [
            {'role': 'system', 'content': 'You are a coding tutor bot to help user write and optimize python code.'},
        ]

    def chat(self, message):
        self.messages.append({'role': 'user', 'content': message})
        response = client.chat.completions.create(model='gpt-3.5-turbo',
        messages=self.messages)
        self.messages.append({'role': 'assistant', 'content': response.choices[0].message.content})
        md_response = response.choices[0].message.content
        CONSOLE.print(Markdown(md_response))
        return md_response


ca = ChatApp()
ca.chat('How do I add two numbers together in python?')
print(None)