import os
import openai
from openai import OpenAI

client = OpenAI(api_key=os.getenv('OPENAI_API_KEY'))

# TODO: The 'openai.organization' option isn't read in the client API. You will need to pass it when you instantiate the client, e.g. 'OpenAI(organization='org-3mutAPfMcU6VbFm5xzs4DKur')'
# openai.organization = 'org-3mutAPfMcU6VbFm5xzs4DKur'

print('*' * 80)

chat_completion = client.chat.completions.create(model='gpt-3.5-turbo', messages=[{'role': 'user', 'content': 'Hello world'}])

# print the chat completion
print(chat_completion.choices[0].message.content)

print('*' * 80)

prompt = "Hello, my name is John and I am a software engineer."
model = "text-davinci-003"
response = client.completions.create(engine=model, prompt=prompt, max_tokens=50)

generated_text = response.choices[0].text
print(generated_text)

print('*' * 80)

prompt = "The quick brown fox"
response = client.completions.create(engine="text-davinci-003",
prompt=prompt,
max_tokens=50)

generated_text = response.choices[0].text.strip()
print(generated_text)

print('*' * 80)

description = "Create a Python script to sort a list of numbers in ascending order."
response = client.completions.create(engine="text-davinci-003",
prompt=f"Code generation:\n{description}",
max_tokens=100)

code = response.choices[0].text.strip()
print(code)

print('*' * 80)

context = "You are chatting with a customer service representative."
message = "Hi, I have a problem with my account."
chat_completion = client.chat.completions.create(model="gpt-3.5-turbo",
max_tokens=50,
messages=[
    {'role': 'system', 'content': context}, 
    {'role': 'user', 'content': message}
    ])

print(chat_completion.choices[0].message.content)

print('*' * 80)