"""
Simple chat GPT RPG story generator.

TODO:
  * Remove all messages from the GM message list when the story is summarised.
  * Add the ability to talk to characters by creating a new chat with a character that is separate from the gm chat.

"""
import os
import re
import json
from typing import Tuple, List, Dict
import time

import openai
from openai import OpenAI

client = OpenAI(api_key=os.getenv('OPENAI_API_KEY'))
import tiktoken

from rich.console import Console
from rich.markdown import Markdown

import prompt


# TODO: The 'openai.organization' option isn't read in the client API. You will need to pass it when you instantiate the client, e.g. 'OpenAI(organization=os.getenv('OPENAI_API_ORG'))'
# openai.organization = os.getenv('OPENAI_API_ORG')


CONSOLE = Console()
MODEL = 'gpt-3.5-turbo'
MAX_N_TOKENS = 4000 # 4096
N_TOKENS_BEFORE_SUMMARISE = 3000


def estimate_token_count(messages: List[Dict[str, str]]):
    """
    Returns the number of tokens used by a list of messages.
    
    gpt-3.5-turbo has an indeterminate number of tokens and it can change, so use gpt-3.5-turbo-0301 instead
    """
    encoding = tiktoken.encoding_for_model('gpt-3.5-turbo-0301')
    tokens_per_message = 4  # every message follows <|start|>{role/name}\n{content}<|end|>\n
    tokens_per_name = -1  # if there's a name, the role is omitted
    num_tokens = 0
    for message in messages:
        num_tokens += tokens_per_message
        for key, value in message.items():
            num_tokens += len(encoding.encode(value))
            if key == "name":
                num_tokens += tokens_per_name
    num_tokens += 3  # every reply is primed with <|start|>assistant<|message|>
    return num_tokens


class ChatAgent:
    def __init__(self, system_prompt, examples: Tuple[str] = None, temperature: float = None):
        # Setting the API key to use the OpenAI API
        self._primer_messages = [
            {'role': 'system', 'content': system_prompt},
        ]
        if examples:
            for example in examples:
                self._primer_messages += [
                {
                    'role': 'system',
                    'name': 'example_user',
                    'content': example[0]
                },
                {
                    'role': 'system',
                    'name': 'example_assistant',
                    'content': example[1]
                }]
        self._temperature = temperature
        self._message_counter = 0
        self._scene_messages = []

    @property
    def _messages(self):
        return self._primer_messages + self._scene_messages

    def chat(self, message: str, role: str = 'user', summary = False):
        self._scene_messages.append({'role': role, 'content': message})

        input_token_count = estimate_token_count(self._messages)
        print(f'Estimated input tokens: {input_token_count}')
        if not summary and input_token_count >= N_TOKENS_BEFORE_SUMMARISE:
            # Too many tokens, summarise.
            self._summarise()

        for _ in range(3):

            try:
                response = client.chat.completions.create(model=MODEL,
                    messages=self._messages,
                    temperature=self._temperature)
            except openai.BadRequestError as e:
                print(e)
                raise e
            except openai.RateLimitError:
                print('Rate limit exceeded. Waiting 60s.')
                time.sleep(60)
                continue
            except Exception as e:
                raise e
            
            if len(response.choices) != 1:
                raise ValueError('Response choices is longer than 1. Why?')

            if response.choices[0].finish_reason == 'length':
                print(response.usage)
                raise ValueError('Maximum input length exceeded.')
            
            if response.choices[0].finish_reason not in ('length', 'stop'):
                raise ValueError(f'Unknown finish_reason {response.choices[0].finish_reason}')
            
            response_tokens = response.usage.completion_tokens
            print(f'Tokens in response: {response_tokens}')
            total_tokens = response.usage.total_tokens
            print(f'Total tokens: {total_tokens}')

            md_response = response.choices[0].message.content
            try:
                json_data, response_with_formatted_json = self._extract_formatted_response(md_response)
            except json.decoder.JSONDecodeError:
                self._scene_messages.append({'role': 'system', 'content': prompt.JSON_RESPONSE_IS_INCORRECT})
                continue

            # # I'm trying to find a bug
            # if any(token in response_with_formatted_json for token in ('\\', '\n')):
            #     print('Is there a pssobile bug here?')

            CONSOLE.print(Markdown(response_with_formatted_json))
            self._scene_messages.append({'role': 'assistant', 'content': response_with_formatted_json})
            return json_data
    
    def interrogate(self, question):
        '''
        Send a yes-or-no question to the agent that is not stored in the message history.

        This can be used to interrogate the agent and ask it for certain facts or opinion.
        '''
        question = question.strip()
        question = ' '.join([question, prompt.INTERROGATION_POSTAMBLE])

        interrogation_messages = [
            {'role': 'system', 'name': 'example_user', 'content': prompt.EXAMPLE_USER_INTERROGATION_1},
            {'role': 'system', 'name': 'example_assistant', 'content': prompt.EXAMPLE_INTERROGATION_RESPONSE_1},
            {'role': 'system', 'name': 'example_user', 'content': prompt.EXAMPLE_USER_INTERROGATION_2},
            {'role': 'system', 'name': 'example_assistant', 'content': prompt.EXAMPLE_INTERROGATION_RESPONSE_2},
            {'role': 'system', 'name': 'example_user', 'content': prompt.EXAMPLE_USER_INTERROGATION_3},
            {'role': 'system', 'name': 'example_assistant', 'content': prompt.EXAMPLE_INTERROGATION_RESPONSE_3},
        ]

        for _ in range(2):
            interrogation_messages.append({'role': 'system', 'content': question})
            try:
                response = client.chat.completions.create(model=MODEL,
                messages=self._messages + interrogation_messages,
                temperature=self._temperature)
            except openai.InvalidRequestError as e:
                print(type(e))
                print(e)
                raise e
            except openai.error.RateLimitError:
                print('Rate limit exceeded. Waiting 20s.')
                time.sleep(20)
            
            raw_response = response.choices[0].message.content
            cleaned_response = raw_response.replace('.', '').upper()

            if 'UNKNOWN' in cleaned_response:
                print(f"WARNING - Agent didn't know the answer to the question. Response: {raw_response}")
                return None
            elif 'NO' in cleaned_response:
                print(f'Response: {raw_response}')
                return False
            elif 'YES' in cleaned_response:
                print(f'Response: {raw_response}')
                return True
            else:
                print(f'WARNING - Agent interrogation failed. Response: "{cleaned_response}". Trying again.')
                question = prompt.INTERROGATION_CORRECTION_PROMPT
                interrogation_messages.append({'role': 'assistant', 'content': raw_response})
        else:
            print(f'ERROR - Agent interrogation failed. Response: {cleaned_response}.')

    def _extract_formatted_response(self, raw_response):
        match = re.search('^[\s\S]*```([\s\S]*)```[\s\S]*$', raw_response)
        if not match:
            print(raw_response)
            raise ValueError('Could not get formatted response from agents raw response.')

        json_str = match.group(1).replace('\n', ' ')
        try:
            json_data = json.loads(json_str)
        except json.decoder.JSONDecodeError:
            print('ERROR - Falied to parse the agents formatted response to json.')
            raise 
        response_with_formatted_json = raw_response.replace(json_str, json.dumps(json_data, indent=2))
        return json_data, response_with_formatted_json
    
    def _summarise(self):
        self.chat(prompt.EXAMPLE_USER_SUMMARISE_STORY, summary=True)
        # Keep the previous message, skip the command to summarise, keep the returned summary, keep the latest message 
        # that has yet to be sent.
        self._scene_messages = [self._scene_messages[-4], self._scene_messages[-1], self._scene_messages[-3]]

class GameMaster(ChatAgent):

    def __init__(self):
        super().__init__(prompt.GAME_MASTER_SYSTEM_PROMPT, 
                         examples=[('Generate the story.', prompt.EXAMPLE_GAME_MASTER_RESPONSE), 
                                   (prompt.EXAMPLE_PLAYER_ACTION_TO_GM, 
                                    prompt.EXAMPLE_GM_RESPONSE_TO_PLAYER_ACTION),
                                   (prompt.EXAMPLE_USER_SUMMARISE_STORY, 
                                    prompt.EXAMPLE_GM_RESPONSE_TO_SUMMARISE_STORY)])
        self._scene = None
        self._characters = None
        self._message_counter = 0

    def chat(self, *args, **kwargs):
        try:
            reponse = super().chat(*args, **kwargs)
        except openai.BadRequestError as e:
            if e.code == 'context_length_exceeded':
                self._summarise()
            else:
                print(e)
                raise e
        
        return reponse

    def start(self):
        response = self.chat('Generate the story.')
        self._scene = response['scene']
        self._characters = response['characters']

    def add_player_action(self, player_action_text):

        if '*' in player_action_text:
            raise ValueError('Invalid character (*) in player action input.')

        if not self._is_player_action_allowed(player_action_text):
            print('That action is not possible or not allowed. Try something else.')
            return 

        player_action_dict = {
            'scene': [{
                'type': 'description', 
                'content': player_action_text
                }]
            }
        GM.chat(json.dumps(player_action_dict))

    def _is_player_action_allowed(self, player_action: str):
        return GM.interrogate(prompt.PLAYER_CAN_DO_ACTION_INTERROGATION.format(player_action))


GM = GameMaster()
GM.start()

while True:
    CONSOLE.print(prompt.GET_INPUT_FROM_PLAYER_PROMPT)
    player_input = input('>')

    if len(player_input) == 0:
        continue

    try:
        if player_input == '1':
            CONSOLE.print(Markdown(f'```{json.dumps(GM._characters, indent=2)}```'))
        elif player_input == '2':
            CONSOLE.print(Markdown(f'```{json.dumps(GM._scene, indent=2)}```'))
        elif player_input == '3':
            interrogation_question = input('INTERROGATE >')
            print(GM.interrogate(interrogation_question))
        elif player_input[0] == '*':
            player_action = player_input[1:]
            GM.add_player_action(player_action)
        else:
            player_speech = player_input
            print('Not implemented yet.')
    except Exception as e:
        print(type(e))
        print(e)
        CONSOLE.print("That response didn't work. Try again.")
